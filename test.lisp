(defun foo()
	(values 3 7))


(defstruct person
	name
	age
	waist-size
	favorite-color)

(defparameter *bob* (make-person	:name "Bob"
									:age 35
									:waist-size 32
									:favorite-color "blue"))

(defparameter *that-guy* #S(PERSON :NAME "Bob" :AGE 36 :WAIST-SIZE 32 :FAVORITE-COLOR "blue"))
