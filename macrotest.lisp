(defmacro let1 (var val &body body)
	`(let ((,var ,val))
		,@body))

(defun add (a b)
	(let ((x (+ a b)))
	(format t "The sum is ~a" x)
	))

(defun add (a b)
	(let1 x (+ a b)
		(format t "The sum is ~a" x)
		x))